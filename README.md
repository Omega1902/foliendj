------------------------------------------------------------------------
README FILE

------------------------------------------------------------------------

PROJEKT NAME: FolienDJ <br>
SINN UND ZWECK: Diashow- und Countdownprogramm für Veranstaltungen <br>
DATUM: 2017 <br>
AUTHOR: Nils Ruppel und Micha Kessler <br>
VERSION: 0.52 <br>
LIZENZ: Free and Open Source (LGPL) <br>
Weiter Lizenten: die Library GSON ist von Google unter der Apache License 2.0 veröffentlicht <br>

------------------------------------------------------------------------

WIE IST DAS PROGRAMM ZU STARTEN: 

- Zuerst muss die Java-Runtime-Umgebung installiert werden, damit die *.jar Datei ausgeführt werden kann.
	-> Dazu entweder im Netz "JRE" (Java Runtime Enviroment) suchen und beispielweise vom Chip.de Server herunterladen
	   oder direkt von Oracle-Website die neuste Version besorgen
	-> WICHTIG: mindestens Version 7 verwenden, sonst "könnte" es sein, dass nicht alle funktionen korrekt funktionieren.

- Wenn die Java-Runtime-Umgebung vorhanden ist, kann die *.jar - Datei, also das Programm (Bsp. FolienDJ_V1.0.jar), 
  mit einem Doppelklick ausgeführt werden.


------------------------------------------------------------------------

BENUTZERHINWEISE/ BEDIENUNGSANLEITUNG:

Dieses Programm ist dazu da Informationen in Bild-Form vor Beginn einer Veranstaltung in einer Diashow-Form weiterzugeben.

Mit dem Programm ist es möglich eine Präsentation von bis zu zehn Bildern als Diashow auf einem gewünschten Bildschirm anzuzeigen.
Außerdem kann ein Countdown, sowohl auf schwarzem Hintergrund (ohne hinterlegte Bilder) oder den Bildern überlagert, angezeigt werden.
In Planung ist die Möglichkeit ein Musikmodul zu integrieren.


Das Programm ist weitestgehend selbsterklärend/ intuitiv ;-)   


Nach Programmstart öffnet sich ein Fenster, welches zwei blau umrandete Felder enthält (Diashow & Hintergrundmusik | Countdown).


- "Diashow & Hintergrundmusik"
	
	- Über Bild hinzufügen lassen sich Bilder (png jpg...) zur Präsentation in die untenstehende Liste hinzufügen.
	- Es lassen sich mit der Maus einzelne Bilder in der Liste auswählen und mit dem Button "Bild löschen" entfernen.
	- Die Anzeigedauer eines Bildes in der Präsentation kann mithilfe der nebenstehenden Tasten "+" und "-" angepasst werden.
	- Über das Auswahlfenster im unteren Bereich kann der Bildschirm, auf dem die Präsentation ablaufen soll gewählt werden.
	- Der BKT (Bildschirm-Konfigurations-Test) - Knopf ist dazu da angeschlossene Bildschirme zu detektieren. 
	  Es erscheint ein kleines Fenster, welches die Anzahl der Bildschirme enthält. (Je nach Version erscheint auf jedem Bildschirm
	  ein kleines Fenster, welches in der Titelzeile die Bezeichnung des jeweiligen Bildschirms anzeigt)
	  Diesen Knopf nur betätigen, wenn die gewünschten Bildschirme nicht erscheinen, oder nachträglich angeschlossen wurden.

- "Countdown"
	
	- Über den runden Knopf kann der Countdown aktiviert werden. Dann werden, wenn vorhanden, den ausgewählten Bildern
	  ein Countdown überlagert.
	- Mit dem Schieberegler kann die Zeit des Endzeitpunktes des Countdowns angepasst werden. Diese wird in einem unterlagerten Text angezeigt.
	- Die Taste Schriftfarbe öffnet ein Farbwahlfenster, durch welches die Schriftfarbe (standartmäßig grau) geändert werden kann.
	- Mit dem Ausklappfenster "Schriftart" kann diese angepasst werden. Hierzu müssen die vorhandenen Standartschriftarten vorhanden sein.
	- Mit den Tasten "+" und "-" kann die Schriftgröße des Countdowns angepasst werden.
	- Die Position des Countdownschriftzuges kann über das nebenstehende Auswahlfenster gewählt werden.
	- Soll der Countdown erst ein paar Minuten der Folien-Diashow überlagert werden, so muss diese Funktion über den runden Knopf 
	  aktiviert werden und die gewünschte Zeit über die Button "+" und "-" eingestellt werden. Dann läuft die Präsentation der Folien 
	  nach dem Starten ab. Hat man beispielweise fünf Minuten eingestellt, so wird der Countdown fünf Minuten vor dem Erreichen der oben 
	  eingestellten Zeit angezeigt.

Im unteren Teil werden zusätzliche Hinweise angezeigt. (Sie ändern sich nicht ständig. Sie können sich also auf vorher getätigte Anpassungen beziehen) 

- "START"  -> Mit diesem Knopf wird eine Präsentation mit den oben getätigten Einstellungen gestartet.

- "STOPP"  -> Mit diesem Knopf lässt sich die Präsentation wieder beenden. Es kann lediglich eine Präsentation gleichzeitig abgespielt werden.
	      Die Päsenation lässt sich auch durch Anklicken der Präsentationsoberfläche mit der linken Maustaste beenden.

------------------------------------------------------------------------

VERSIONSVERZEICHNIS:

!!! Die Versionen die mit einer Null (V0.XX) beginnen sind Prototypen und können fehlerhaftes Verhalten aufweisen.

!!! Änderungen werden erst ab der Version V1.0 hier vermerkt.


------------------------------------------------------------------------

ANREGUNGEN / GEPLANTE ÄNDERUNGEN:

Funktional:

- Hintergrundmusik
- Bildposition auswählbar gestallten
- Musikmodul integrieren

Konzeptionell/ Codespezifisch:

- Abfangen von Exception bei Abbruch der Bildauswahl
- Abfangen von Exception bei Drücken des Bild-Löschen-Buttons, ohne, dass ein Bild ausgewählt wurde
- Bilder skalieren funktioniert, prüfen ob mit großen Dateien möglich

-------------------------------------------------------------------------

Versionsmanagement:

V0.52 Reparieren des zu großen Fensters für die Diashow 
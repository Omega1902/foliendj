/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;
/**
 * 
 * 
 * @author N R
 * @version 1.0
 */
//import java.util.*;
import javax.swing.JPanel;

public class PicturePanel extends JPanel {
	/** generated */
	private static final long serialVersionUID = 414029334955434549L;
	private int aktBild = 0;
	private ScaleImage imgArray;
	private Position picPosition;
	private Dimension screensize;
	private int x = 0;
	private int y = 0;

	public PicturePanel(Dimension dim, FolienSettings folienSettings) {
		imgArray = new ScaleImage(folienSettings.getPics(), dim, folienSettings.getPicSizePercent());
		this.picPosition = folienSettings.getPicPosition();
		screensize = dim;
		init();
	}

	private void init() {
		////// Panel-Setting:
		setDoubleBuffered(true);
		setBackground(Color.BLACK);
		setLocation(0, 0);
		setSize(screensize);
		setAlignmentX(JComponent.CENTER_ALIGNMENT);
		setAlignmentY(JComponent.CENTER_ALIGNMENT);
		setVisible(true);
		zeichne();
	}

	public void zeichne() {
		if (imgArray.getSize() > 0) {
			aktBild = (aktBild + 1) % imgArray.getSize(); 
			positionTransform();
		}
		repaint();
	}

	private void positionTransform() {
		switch (picPosition) {
		case OBEN_LINKS:
			setLeft();
			setTop();
			break;
		case OBEN_ZENTRIERT:
			setCenterH();
			setTop();
			break;
		case OBEN_RECHTS:
			setRight();
			setTop();
			break;
		case MITTE_LINKS:
			setLeft();
			setCenterV();
			break;
		case MITTE_ZENTRIERT:
			setCenterH();
			setCenterV();
			break;
		case MITTE_RECHTS:
			setRight();
			setCenterV();
			break;
		case UNTEN_LINKS:
			setLeft();
			setBottom();
			break;
		case UNTEN_ZENTRIERT:
			setCenterH();
			setBottom();
			break;
		case UNTEN_RECHTS:
			setRight();
			setBottom();
			break;
		default:
			setLeft();
			setTop();
			break;
		}
	}

	private void setBottom() {
		y = (int) getEnd(screensize.getHeight(), imgArray.getDim(aktBild).getHeight());
	}

	private void setCenterV() {
		y = (int) getCentered(screensize.getHeight(), imgArray.getDim(aktBild).getHeight());
	}

	private void setRight() {
		x = (int) getEnd(screensize.getWidth(), imgArray.getDim(aktBild).getWidth());
	}

	private void setCenterH() {
		x = (int) getCentered(screensize.getWidth(), imgArray.getDim(aktBild).getWidth());
	}

	private void setLeft() {
		x = 0;
	}

	private void setTop() {
		y = 0;
	}

	private double getCentered(double outerValue, double innerValue) {
		return (outerValue - innerValue) / 2;
	}

	private double getEnd(double outerValue, double innerValue) {
		return outerValue - innerValue;
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;

		if (imgArray.getImage(aktBild) != null)
			g2.drawImage(imgArray.getImage(aktBild), x, y, null);
	}
}
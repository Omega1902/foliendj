/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;

import javax.swing.JFrame;

public class Screendetection {
	private int numberOfScreens;
	private GraphicsEnvironment env;
	private GraphicsDevice[] devices;

	public Screendetection() {
		try {
			// Get local graphics environment:
			env = GraphicsEnvironment.getLocalGraphicsEnvironment();

			// Returns an array of all of the screen GraphicsDevice objects:
			devices = env.getScreenDevices();
			// System.out.println("Bilschirm 1 = " + devices[0]);

			numberOfScreens = devices.length;
			// System.out.println("Anzahl der angeschlossenen Bilschirme = " +
			// numberOfScreens);
		} catch (HeadlessException e) {
			System.out.println("Fehler beim Ermitteln der Bildschirmanzahl!!!");
		}
	}

	public int getNumberOfScreens() {
		return numberOfScreens;
	}

	public GraphicsDevice[] getScreens() {
		return devices;
	}

	public GraphicsDevice getScreen(int arrayNumber) {
		return devices[arrayNumber];
	}

	public void screenTest() {
		// Get local graphics environment:
		env = GraphicsEnvironment.getLocalGraphicsEnvironment();

		// Returns an array of all of the screen GraphicsDevice objects:
		devices = env.getScreenDevices();
		numberOfScreens = devices.length;

		javax.swing.JOptionPane.showConfirmDialog((java.awt.Component) null, "Found : " + devices.length,
				"screen detected ?", javax.swing.JOptionPane.DEFAULT_OPTION);

		// *
		for (int j = 0; j < devices.length; j++) {
			GraphicsDevice gd = devices[j];
			JFrame frame = new JFrame(gd.getDefaultConfiguration());
			frame.setTitle("Bildschrim Nr. " + (j + 1));
			frame.setSize(300, 50);
			frame.setVisible(true);
		}
		// */
	}
}
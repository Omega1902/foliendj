/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

public enum Position {
	OBEN_LINKS("Oben - links"), OBEN_ZENTRIERT("Oben - zentriert"), OBEN_RECHTS("Oben - rechts"), MITTE_LINKS(
			"Mitte - links"), MITTE_ZENTRIERT("Mitte - zentriert"), MITTE_RECHTS("Mitte - rechts"), UNTEN_LINKS(
					"Unten - links"), UNTEN_ZENTRIERT("Unten - zentriert"), UNTEN_RECHTS("Unten - rechts");
	public final String label;

	private Position(String label) {
		this.label = label;
	}

	public static Position getPosition(String positionLabel) {
		for (Position pos : Position.values()) {
			if (pos.label.equals(positionLabel)) {
				return pos;
			}
		}
		throw new RuntimeException("No Position found!");
	}

	public static String[] getPositionLabels() {
		String[] result = new String[Position.values().length];
		for (int i = 0; i < Position.values().length; i++)
			result[i] = (Position.values())[i].label;
		return result;
	}

	public String toString() {
		return label;
	}
}

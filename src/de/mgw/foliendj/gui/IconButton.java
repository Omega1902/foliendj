/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj.gui;

import java.awt.Dimension;

import javax.swing.JButton;

public class IconButton extends JButton {
	/**generated*/
	private static final long serialVersionUID = 7508773568040884883L;
	public static final char PLUS = '+';
	public static final char MINUS = '-';
	private static final Dimension smallDimension = new Dimension(45,25);
	
	public IconButton(char text){
		setText(text + "");
		setPreferredSize(smallDimension);
	}
}

/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj.gui;

import java.io.File;

/**
 * @author N R 
 * @version 1.0
 */
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Filechooser {
	private int flag;
	private JFileChooser chooser;

	public Filechooser(String lastDir) {
		// Filter verbessern:

		FileFilter filter = new FileNameExtensionFilter("Bilder", "gif", "png", "jpg");
		if (lastDir == null || "".equals(lastDir)) {
			chooser = new JFileChooser(System.getProperty("user.home"));
		} else {
			chooser = new JFileChooser(lastDir);
		}
		chooser.setFileFilter(filter);
		chooser.setMultiSelectionEnabled(true);

		flag = chooser.showDialog(null, "Auswählen");
	}

	/**
	 * 
	 * @return all selected files
	 */
	public File[] getFiles(){
		return chooser.getSelectedFiles();
	}
	
	/**
	 * 
	 * @return directory of selected file
	 */
	public String getFolder() {
		return fileChosen() ? chooser.getSelectedFile().getAbsolutePath() : null;
	}

	public boolean fileChosen() {
		return flag == JFileChooser.APPROVE_OPTION;
	}
	
	public int getAnz(){
		return fileChosen() ? -1 : getFiles().length;
	}
}

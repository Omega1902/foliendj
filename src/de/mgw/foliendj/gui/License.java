/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class License extends JFrame {
	/**generated*/
	private static final long serialVersionUID = 4189069751414849251L;
	private JPanel panel;
	private JPanel bottomPanel;
	private JEditorPane licenseText;
	private JScrollPane editorScrollPane;
	private JButton accept;
	private JButton deny;
	private boolean accepted = false;
	
	public License(String licenses){
		super("Lizenz");

		panel = new JPanel();
		bottomPanel = new JPanel();
		licenseText = new JEditorPane();
		licenseText.setEditable(false);
		licenseText.setText(licenses);
		editorScrollPane = new JScrollPane(licenseText);
		editorScrollPane.setVerticalScrollBarPolicy(
		                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		editorScrollPane.setPreferredSize(new Dimension(1000, 700));
		editorScrollPane.setMinimumSize(new Dimension(10, 10));
		
		

		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		accept = new JButton("Lizenz akzeptieren");
		deny = new JButton("Lizenz nicht akzeptieren");

		bottomPanel.add(accept);
		bottomPanel.add(deny);
		panel.add(editorScrollPane);
		panel.add(bottomPanel);

		add(panel);
		
		ButtonListener butList = new ButtonListener();

		accept.addActionListener(butList);
		deny.addActionListener(butList);
		
		pack();
		setVisible(true);
	}
	
	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == accept) {
				accepted = true;
				System.out.println("license accepted");
			}else{
				System.out.println("license not accepted");
			}
			setVisible(false); //you can't see me!
			dispose(); //Destroy the JFrame object
		}
	}

	public boolean isAccepted() {
		return accepted;
	}
}

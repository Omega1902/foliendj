/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Scrollbar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.UIManager;

import de.mgw.foliendj.Controller;
import de.mgw.foliendj.FilePath;
import de.mgw.foliendj.Fonts;
import de.mgw.foliendj.Position;
import de.mgw.foliendj.Utils;

public class Gui extends JFrame {
	/** generated */
	private static final long serialVersionUID = -651171596024745934L;

	private Controller controller = new Controller();

	private Container contPane = getContentPane();
	private JPanel titelPanel = new JPanel();
	private JPanel unteresPanel = new JPanel();
	private JPanel infoPanel = new JPanel();
	private JPanel timerPanel = new JPanel();
	private JPanel picturePanel = new JPanel();
	private JPanel screenPanel = new JPanel();
	private JPanel screenAndPicturePanel = new JPanel();
	private JPanel southPanel = new JPanel();
	private JPanel picPositionPanel = new JPanel();
	private JToggleButton countdownStatus = new JToggleButton("Countdown");

	private JLabel title = new JLabel("FolienDJ");

	private JButton start = new JButton("START");
	private JButton stopp = new JButton("STOPP");
	private JButton reset = new JButton("Standarteinstellungen");
	private JLabel infoMeldung = new JLabel(Controller.ALLES_OK);

	private JButton picSelect = new JButton("Bild hinzufügen");
	private JButton picDisselect = new JButton("Bild löschen");
	private JButton save = new JButton("Speichern");
	private JButton load = new JButton("Laden");
	private JPanel picButtonPanel = new JPanel();
	private JPanel picPanel = new JPanel();
	private BilderListe picListPanel = new BilderListe();
	private JPanel bildDauerPanel = new JPanel();
	private JButton bildDauerDown = new IconButton(IconButton.MINUS);
	private JButton bildDauerUp = new IconButton(IconButton.PLUS);
	private JLabel bildDauerLabel = new JLabel();
	private JPanel screenSelectPanel = new JPanel();
	private JPanel saveAndLoadPanel = new JPanel();

	private JToggleButton outlineStatus = new JToggleButton("Schriftumrandung");

	private JComboBox<String> screenSelect = new JComboBox<String>();
	private JLabel screenSelectLabel = new JLabel("Präsentation läuft auf folgendem Bildschirm ab: ");
	private JButton detectScreen = new JButton("scan");
	private JPanel picSouthPanel = new JPanel();

	private JLabel time = new JLabel();
	private JLabel picPositionLabel = new JLabel("Bildposition:    ");
	private JLabel fontTypeLabel = new JLabel("Schriftart:");
	private JLabel fontSizeLabel = new JLabel();
	private JButton sizeDown = new IconButton(IconButton.MINUS);
	private JButton sizeUp = new IconButton(IconButton.PLUS);
	private JLabel positionLabel = new JLabel("Position:    ");
	private JLabel timeTriggerLabel = new JLabel("");
	private JButton timeTriggerDown = new IconButton(IconButton.MINUS);
	private JButton timeTriggerUp = new IconButton(IconButton.PLUS);
	private JToggleButton timeTriggerStatus = new JToggleButton("Verzögerter Beginn");
	private JButton fontColor = new JButton("Schriftfarbe auswählen");
	private Scrollbar scTime = new Scrollbar(Scrollbar.HORIZONTAL, 0, 5, 0, 293);
	private JLabel bildGroesseLabel = new JLabel();
	private JButton bildGroesseDown = new IconButton(IconButton.MINUS);
	private JButton bildGroesseUp = new IconButton(IconButton.PLUS);
	private JPanel bildGroessePanel = new JPanel();
	private JPanel fontTypePanel = new JPanel();
	private JPanel fontSizePanel = new JPanel();
	private JPanel positionPanel = new JPanel();
	private JPanel timeTriggerPanel = new JPanel();
	private JPanel scPanel = new JPanel();
	private JComboBox<Fonts> fontSelect = new JComboBox<Fonts>(Fonts.values());
	private JComboBox<Position> positionSelect = new JComboBox<Position>(Position.values());
	private JComboBox<Position> picPositionSelect = new JComboBox<Position>(Position.values());

	private final Color BackgroundColor = Color.WHITE;
	private String lastDir = null;

	// private Insets myInsets = new Insets(1, 1, 1, 1);

	public Gui() {
		// .setBorder(BorderFactory.createTitledBorder("Name")); //für eine
		// beschriftete Border
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// * ContentPane - Settings
		// *////////////////////////////////////////////////////////////////////
		contPane.setLayout(new BorderLayout(10, 10));
		contPane.setBackground(BackgroundColor);
		contPane.add(southPanel, BorderLayout.SOUTH);
		contPane.add(timerPanel, BorderLayout.EAST);
		contPane.add(titelPanel, BorderLayout.NORTH);
		contPane.add(screenAndPicturePanel, BorderLayout.WEST);
		// contPane.setSize(1800, 600);

		// * PanelLayout - Settings
		// *////////////////////////////////////////////////////////////////////
		titelPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		unteresPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		infoPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		timerPanel.setLayout(new GridLayout(10, 1, 2, 2));
		southPanel.setLayout(new BorderLayout(5, 5));
		screenAndPicturePanel.setLayout(new BorderLayout());
		picPositionPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		picturePanel.setLayout(new BorderLayout());
		screenPanel.setLayout(new BoxLayout(screenPanel, BoxLayout.PAGE_AXIS));
		picButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		picPanel.setLayout(new BorderLayout());
		bildDauerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		screenSelectPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		bildGroessePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		saveAndLoadPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		picSouthPanel.setLayout(new BoxLayout(picSouthPanel, BoxLayout.PAGE_AXIS));

		// * Titel Panel
		// *//////////////////////////////////////////////////////////////////////////////
		title.setFont(new Font("Magneto", Font.BOLD, 28));
		titelPanel.add(title);
		titelPanel.setBackground(new Color(140, 76, 57));

		// * Unteres Panel
		// *//////////////////////////////////////////////////////////////////////////////
		unteresPanel.add(start);
		unteresPanel.add(stopp);
		infoPanel.add(infoMeldung);
		southPanel.add(infoPanel, BorderLayout.NORTH);
		southPanel.add(unteresPanel, BorderLayout.SOUTH);
		southPanel.setBackground(BackgroundColor);
		unteresPanel.setBackground(BackgroundColor);
		infoPanel.setBackground(BackgroundColor);

		// * Rechtes Panel - Timer Panel
		// *//////////////////////////////////////////////////////////////
		timerPanel.setBorder(BorderFactory.createTitledBorder("Countdown"));
		timerPanel.setPreferredSize(new Dimension(280, 430));
		// timerPanel.add(timerHeader);
		scTime.setPreferredSize(new Dimension(180, 20));
		scTime.setBlockIncrement(12);
		scPanel.setMaximumSize(new Dimension(200, 50));
		timerPanel.add(countdownStatus);
		scPanel.add(scTime);
		timerPanel.add(scPanel);
		timerPanel.add(time);
		fontTypePanel.add(fontTypeLabel);
		fontTypePanel.add(fontSelect);
		timerPanel.add(fontColor);
		timerPanel.add(fontTypePanel);
		timerPanel.add(outlineStatus);
		fontSizePanel.add(fontSizeLabel);
		fontSizePanel.add(sizeUp);
		fontSizePanel.add(sizeDown);
		timerPanel.add(fontSizePanel);
		// timerPanel.add(actTime);
		positionPanel.add(positionLabel);
		positionPanel.add(positionSelect);
		timerPanel.add(positionPanel);
		timerPanel.add(timeTriggerStatus);
		timeTriggerPanel.add(timeTriggerLabel);
		timeTriggerPanel.add(timeTriggerUp);
		timeTriggerPanel.add(timeTriggerDown);
		timerPanel.add(timeTriggerPanel);

		// * Linkes Panel - Bild/Musik - Panel
		// *////////////////////////////////////////////////////////
		screenAndPicturePanel.add(screenPanel, BorderLayout.NORTH);
		screenAndPicturePanel.add(picturePanel, BorderLayout.SOUTH);

		screenPanel.setBorder(BorderFactory.createTitledBorder("Bildschirm"));
		screenPanel.add(screenSelectPanel);
		screenPanel.add(saveAndLoadPanel);
		screenSelectPanel.add(screenSelectLabel);
		screenSelectPanel.add(screenSelect);
		screenSelectPanel.add(detectScreen);
		fillScreenJComboBox(controller.getNumberOfScreens());
		saveAndLoadPanel.add(save);
		saveAndLoadPanel.add(load);
		saveAndLoadPanel.add(reset);

		picturePanel.setBorder(BorderFactory.createTitledBorder("Diashow"));
		picturePanel.add(picButtonPanel, BorderLayout.NORTH);
		picturePanel.add(picPanel, BorderLayout.SOUTH);
		picturePanel.setPreferredSize(new Dimension(645, 350));// (645, 400));
		picButtonPanel.add(picSelect);
		picButtonPanel.add(picDisselect);

		// picPositionPanel.add(picPositionSelect);
		picPanel.add(picListPanel, BorderLayout.NORTH);
		picPositionPanel.add(picPositionLabel);
		picPositionPanel.add(picPositionSelect);

		bildGroessePanel.add(bildGroesseLabel);
		bildGroessePanel.add(bildGroesseUp);
		bildGroessePanel.add(bildGroesseDown);

		// picPanel.add(comp,
		// constraints);picPositionPanel.add(picPositionSelect);
		// picPositionPanel.add(picPositionLabel);
		picPanel.add(picSouthPanel, BorderLayout.SOUTH);
		picSouthPanel.add(picPositionPanel);
		picSouthPanel.add(bildGroessePanel);
		picSouthPanel.add(bildDauerPanel);
		bildDauerPanel.add(bildDauerLabel);
		bildDauerPanel.add(bildDauerUp);
		bildDauerPanel.add(bildDauerDown);

		// * Listener
		// *///////////////////////////////////////////////////////////////////////////////
		ButtonListener butList = new ButtonListener();

		scTime.addAdjustmentListener(new ValueListener());
		start.addActionListener(butList);
		stopp.addActionListener(butList);
		reset.addActionListener(butList);
		countdownStatus.addActionListener(butList);
		picSelect.addActionListener(butList);
		picDisselect.addActionListener(butList);
		save.addActionListener(butList);
		load.addActionListener(butList);
		fontSelect.addActionListener(butList);
		sizeDown.addActionListener(butList);
		sizeUp.addActionListener(butList);
		positionSelect.addActionListener(butList);
		picPositionSelect.addActionListener(butList);
		timeTriggerStatus.addActionListener(butList);
		timeTriggerUp.addActionListener(butList);
		timeTriggerDown.addActionListener(butList);
		bildDauerUp.addActionListener(butList);
		bildDauerDown.addActionListener(butList);
		bildGroesseUp.addActionListener(butList);
		bildGroesseDown.addActionListener(butList);
		screenSelect.addActionListener(butList);
		fontColor.addActionListener(butList);
		detectScreen.addActionListener(butList);
		outlineStatus.addActionListener(butList);
		// addKeyListener(new EscListener());
		// countdownStatus.setMargin(myInsets);
		// * JFrame - Einstellungen
		// *//////////////////////////////////////////////////////////////////////
		setTitle("FolienDJ Version: " + Utils.PROGRAM_VERSION); // Schriftzug in
																// Titelzeile
																// einfügen
		setBackground(BackgroundColor); // Hintergrundfarbe ändern, auch für
										// Panels
		setLocation(20, 20); // optional
		// setSize(950, 580); // alternativ pack();
		pack();
		setResizable(false);// Verhindert, dass Größe geändert wird
		updateAll();
		try {
			InputStream stream = Gui.class.getClassLoader().getResourceAsStream("images/favicon.png");
			BufferedImage image = ImageIO.read(stream);
			if (image != null)
				System.out.println("Seems good for icon.");
			setIconImage(image);
		} catch (Exception e) {
			e.printStackTrace();
		}
		setVisible(true);
	}

	private void updateAll() {
		countdownStatus.setSelected(controller.getCountDown());
		timeTriggerStatus.setSelected(controller.getTimeTriggerStatus());
		positionSelect.setSelectedItem(controller.getPosition());
		picPositionSelect.setSelectedItem(controller.getPicPosition());
		fontSelect.setSelectedItem(controller.getFont());
		picListPanel.addNewList(controller.getPics());
		infoMeldung.setText(controller.getInfo());
		scTime.setValue(controller.getMinutes());
		outlineStatus.setSelected(controller.getFontOutline());

		updateSchriftgroesseString();
		updateBildDauerLabel();
		updateTimeTriggerLabel();
		updateCountdownLabel();
		updatePicSizeLabel();
		countdownGUISetEnabledState();
		listGUISetEnabledState();
	}

	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == start) {
				controller.start();
			} else if (e.getSource() == stopp) {
				controller.stop();
			} else if (e.getSource() == reset) {
				controller.reset();
				updateAll();
			} else if (e.getSource() == countdownStatus) {
				controller.setCountDown(countdownStatus.isSelected());
				countdownGUISetEnabledState();
			} else if (e.getSource() == fontSelect) {
				controller.setFont((Fonts) fontSelect.getSelectedItem());
			} else if (e.getSource() == positionSelect) {
				controller.setPosition((Position) positionSelect.getSelectedItem());
			} else if (e.getSource() == picPositionSelect) {
				controller.setPicPosition((Position) picPositionSelect.getSelectedItem());
			} else if (e.getSource() == screenSelect) {
				controller.setScreen(screenSelect.getSelectedIndex());
				updateAll();
			} else if (e.getSource() == timeTriggerStatus) {
				controller.setTimeTriggerStatus(timeTriggerStatus.isSelected());
				delayGUISetEnabledState();
			} else if (e.getSource() == outlineStatus) {
				controller.setFontOutline(outlineStatus.isSelected());
			} else if (e.getSource() == timeTriggerUp) {
				controller.timeTriggerUp();
				updateTimeTriggerLabel();
			} else if (e.getSource() == timeTriggerDown) {
				controller.timeTriggerDown();
				updateTimeTriggerLabel();
			} else if (e.getSource() == picSelect) {
				picListPanel.addNewList(controller.picSelect(picSelect()));
				listGUISetEnabledState();
			} else if (e.getSource() == picDisselect) {
				picListPanel.addNewList(controller.picDisselect(picListPanel.selection()));
				listGUISetEnabledState();
			} else if (e.getSource() == sizeUp) {
				controller.fontSizeUp();
				updateSchriftgroesseString();
			} else if (e.getSource() == sizeDown) {
				controller.fontSizeDown();
				updateSchriftgroesseString();
			} else if (e.getSource() == bildDauerUp) {
				controller.picDurationUp();
				updateBildDauerLabel();
			} else if (e.getSource() == bildDauerDown) {
				controller.picDurationDown();
				updateBildDauerLabel();
			} else if (e.getSource() == bildGroesseUp) {
				controller.picSizeUp();
				updatePicSizeLabel();
			} else if (e.getSource() == bildGroesseDown) {
				controller.picSizeDown();
				updatePicSizeLabel();
			} else if (e.getSource() == fontColor) {
				controller.setFontColor(new ColorChoose(controller.getFontColor()).getSchriftfarbe());
			} else if (e.getSource() == detectScreen) {
				fillScreenJComboBox(controller.detectScreen());
			} else if (e.getSource() == load) {
				String temp = getLoadName();
				controller.loadFromFile(temp);
				updateAll();
			} else if (e.getSource() == save) {
				String temp = getSaveName();
				controller.saveAs(temp);
			}
			infoMeldung.setText(controller.getInfo());
		}
	}

	private class ValueListener implements AdjustmentListener {
		public void adjustmentValueChanged(AdjustmentEvent e) {
			controller.setMinutes(scTime.getValue());
			updateCountdownLabel();
		}
	}

	private void fillScreenJComboBox(int anz) {
		screenSelect.removeAllItems();
		for (int i = 0; i < anz; i++) {
			screenSelect.addItem("Bildschirm " + (i + 1));
		}
	}

	@Override
	protected void processWindowEvent(WindowEvent e) {
		if (e.getID() == WindowEvent.WINDOW_CLOSING) {
			controller.saveAll();
		}
		super.processWindowEvent(e);
	}

	private void updateSchriftgroesseString() {
		fontSizeLabel.setText("Schriftgröße: " + controller.getFontSize() + "  ");
	}

	private void updateBildDauerLabel() {
		bildDauerLabel.setText("Die Bilder werden alle " + controller.getPicDuration() + " Sekunde"
				+ (controller.getPicDuration() > 1 ? "n" : "") + " gewechselt");
	}

	private void updateTimeTriggerLabel() {
		timeTriggerLabel.setText("Nur die Letzten " + controller.getTimeTrigger() / 60 + " min");
	}

	private void updateCountdownLabel() {
		time.setText("   Countdown endet um: " + controller.getSelectedHour() + ":"
				+ (controller.getSelectedMin() < 10 ? "0" : "") + controller.getSelectedMin());
	}

	private void updatePicSizeLabel() {
		bildGroesseLabel.setText("Bildgröße: " + controller.getPicSizePercent() + " %");
	}

	public List<FilePath> picSelect() {
		List<FilePath> result = null;
		Filechooser temp = new Filechooser(lastDir);
		if (temp.fileChosen()) {
			File[] filesTemp = temp.getFiles();
			result = new ArrayList<FilePath>(filesTemp.length);
			for(File fileTemp : filesTemp){
				result.add(new FilePath(fileTemp));
			}
			lastDir = temp.getFolder();
		}
		return result;
	}

	private String getSaveName() {
		Object[] possibilities = null;
		return (String) JOptionPane.showInputDialog(null,
				"Aktuelle Bildschirmeinstellung speichern als: \n(Für Standard-Profil \"" + Utils.FALLBACK_FILE_NAME
						+ "\" eingeben)",
				"Speichern...", JOptionPane.PLAIN_MESSAGE, UIManager.getIcon("OptionPane.questionIcon"), // JOptionPane.QUESTION_MESSAGE,
				possibilities, "");

	}

	private String getLoadName() {
		List<String> loadAbles = controller.getLoadAbles();
		for (Iterator<String> iterator = loadAbles.iterator(); iterator.hasNext();) {
			String string = iterator.next();
			if (string.equals(Utils.FALLBACK_FILE_NAME) || string.matches(Utils.SYSTEM_FILE_NAME + "\\d*")) {
				iterator.remove();
			}
		}
		if (loadAbles.size() > 0) {
			Object[] possibilities = loadAbles.toArray();
			return (String) JOptionPane.showInputDialog(null,
					"Wähle die zu ladende Datei für die aktuellen Birdschirmeinstellung aus:", "Laden",
					JOptionPane.PLAIN_MESSAGE, UIManager.getIcon("OptionPane.questionIcon"), // JOptionPane.QUESTION_MESSAGE,
					possibilities, loadAbles.get(0).toString());
		}
		return null;
	}

	private void countdownGUISetEnabledState() {
		boolean enabledState = controller.getCountDown();
		timeTriggerStatus.setEnabled(enabledState);
		positionSelect.setEnabled(enabledState);
		fontSelect.setEnabled(enabledState);
		scTime.setEnabled(enabledState);
		outlineStatus.setEnabled(enabledState);
		fontColor.setEnabled(enabledState);
		sizeDown.setEnabled(enabledState);
		sizeUp.setEnabled(enabledState);

		// also disable labels

		fontTypeLabel.setEnabled(enabledState);
		fontSizeLabel.setEnabled(enabledState);
		time.setEnabled(enabledState);
		positionLabel.setEnabled(enabledState);
		delayGUISetEnabledState();
	}

	private void delayGUISetEnabledState() {
		boolean enabledState = controller.getCountDown() && controller.getTimeTriggerStatus();
		timeTriggerDown.setEnabled(enabledState);
		timeTriggerUp.setEnabled(enabledState);
		timeTriggerLabel.setEnabled(enabledState);
	}

	private void listGUISetEnabledState() {
		picSelect.setEnabled(controller.getPicsSize() < Utils.MAX_Bilder);
		picDisselect.setEnabled(controller.getPicsSize() > 0);

		picTimeGUISetEnabledState();
	}

	private void picTimeGUISetEnabledState() {
		boolean enabledState = controller.getPicsSize() > 1;
		bildDauerLabel.setEnabled(enabledState);
		bildDauerDown.setEnabled(enabledState);
		bildDauerUp.setEnabled(enabledState);
	}
}

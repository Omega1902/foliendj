/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.datatransfer.Transferable;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;
import javax.swing.border.LineBorder;

import de.mgw.foliendj.FilePath;
import de.mgw.foliendj.Utils;

/**
 * Ein Panel, das eine Liste enthält, welche mit den darzustellenden Bildern
 * gefüllt werden kann.
 * 
 * @author N R
 * @version 1.0
 */

public class BilderListe extends JPanel {
	/** generated */
	private static final long serialVersionUID = -1723732042177670478L;
	private JList<FilePath> picList;

	public BilderListe() {
		setLayout(new BorderLayout());
		picList = listInit();
		add(picList, BorderLayout.NORTH);
		setSize(640, 300);

		// picList.setDropTarget(new DropTarget());
	}

	private JList<FilePath> listInit() {
		/*
		 * Vector<MyFile> temp = new Vector<MyFile>(Settings.MAX_Bilder); for
		 * (int i = 0; i < Settings.MAX_Bilder; i++) {
		 * temp.addElement(MyFile.NO_FILE); }
		 */
		JList<FilePath> pics = new JList<FilePath>();// new DNDList<MyFile>(temp);
		pics.setLayoutOrientation(JList.VERTICAL);
		pics.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		pics.setBorder(new LineBorder(Color.BLACK));
		// pics.setDropMode(DropMode.ON_OR_INSERT);
		// pics.setTransferHandler(new MyTransferHandler());
		// pics.setDragEnabled(true);
		return pics;
	}

	public void addNewList(Vector<FilePath> list) {
		picList.setVisible(false);

		picList.removeAll();
		while (list.size() < Utils.MAX_Bilder)
			list.add(FilePath.NO_FILE);
		picList.setListData(list);

		picList.setVisible(true);
	}

	public int selection() {
		return picList.getSelectedIndex();
	}

	private class MyTransferHandler extends TransferHandler {
		/** generated */
		private static final long serialVersionUID = -2198850534160749007L;

		@Override
		public boolean canImport(TransferHandler.TransferSupport support) {
			System.out.println("Checked");
			return true;
		}

		@Override
		public boolean importData(TransferHandler.TransferSupport support) {
			System.out.println("Transfer:");
			System.out.println(support.getTransferable());
			return false;
		}

		@Override
		protected Transferable createTransferable(JComponent c) {
			/*
			 * JList list = (JList) c; List<File> files = new ArrayList<File>();
			 * for (Object obj: list.getSelectedValues()) {
			 * files.add((File)obj); } return new FileTransferable(files);
			 */
			System.out.println("CreateTransferable");
			return null;
		}

		@Override
		public int getSourceActions(JComponent c) {
			System.out.println("getSourceActions");
			return 0;

		}
	}
	/*
	 * private List picList = null;
	 * 
	 * public BilderListe() { setLayout(new BorderLayout()); picList =
	 * listInit(); add(picList, BorderLayout.NORTH); setSize(640, 300);
	 * 
	 * ListListener listener = new ListListener();
	 * picList.addActionListener(listener); picList.setDropTarget(new
	 * DropTarget()); }
	 * 
	 * private List listInit() { List pics = new List(Settings.MAX_Bilder,
	 * false);
	 * 
	 * for (int i = 0; i < Settings.MAX_Bilder; i++) { pics.add(null, i); }
	 * 
	 * return pics; }
	 * 
	 * public void addNewList(String[][] temp) { picList.setVisible(false);
	 * 
	 * picList.removeAll();
	 * 
	 * for (int i = 0; i < Settings.MAX_Bilder; i++) { picList.add(temp[i][1],
	 * i); }
	 * 
	 * picList.setVisible(true); }
	 * 
	 * private class ListListener implements ActionListener { public void
	 * actionPerformed(ActionEvent e) { picList.getSelectedIndex(); } }
	 * 
	 * public int selection() { return picList.getSelectedIndex(); }
	 */
}

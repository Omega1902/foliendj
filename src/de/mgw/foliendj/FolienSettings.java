/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

public class FolienSettings {
	private boolean countDownstatus;
	private boolean timeTriggerStatus;
	private Position position;
	private Position picPosition;
	private Fonts font;
	private Color fontColor;
	private int fontSize;
	private List<FilePath> pics;
	private int picDuration;
	private int selectedHour;
	private int selectedMin;
	private int timeTrigger;
	private int picSizePercent;
	private boolean fontOutline;
	public static final int MAX_PIC_SIZE_PERCENT = 100;
	public static final int MIN_PIC_SIZE_PERCENT = 10;
	public static final int MIN_FONT_SIZE = 10;
	public static final int MIN_PIC_DURATION = 1;
	private static final TypeAdapter<FolienSettings> converter = new Gson().getAdapter(FolienSettings.class);

	public FolienSettings() {
		countDownstatus = false;
		timeTriggerStatus = false;
		position = Position.OBEN_LINKS;
		picPosition = Position.MITTE_ZENTRIERT;
		font = Fonts.Agency_FB;
		fontColor = Color.GRAY;
		fontSize = 100;
		pics = new LinkedList<FilePath>();
		picDuration = 10;
		picSizePercent = 100;
		selectedHour = 10;
		selectedMin = 0;
		timeTrigger = 5 * 60;
		fontOutline = true;
	}

	public static void saveTo(FolienSettings settings, String name) {
		String json = converter.toJson(settings);
		// actual save
		Writer out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(Utils.PROFILE_DIR + name + Utils.FILE_END), Utils.CHARSET));
			out.write(json);
		} catch (Exception e) {
			System.err.println("Error when saving to file: " + e.getMessage());
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void saveAll(FolienSettings[] settings) {
		for (int i = 0; i < settings.length; i++) {
			saveTo(settings[i], Utils.SYSTEM_FILE_NAME + i);
		}
	}

	/**
	 * Returns the default FolienSettings, which is the "standard" file. If it does not exist, it will be created.
	 * 
	 * @return standard FolienSettings
	 */
	public static FolienSettings loadDefault() {
		FolienSettings toReturn;
		try {
			toReturn = loadFromFile(Utils.getFallbackName());
		} catch (Exception ee) {// just create with defaults
			toReturn = new FolienSettings();
			System.err.println("Error when loading from file " + Utils.getFallbackName() + ": " + ee.getMessage());
			saveTo(toReturn, Utils.FALLBACK_FILE_NAME);
		}
		return toReturn;
	}

	/**
	 * Loads the FolienSettings of the saved screen, given as integer, starting by 0
	 * 
	 * @param numberOfScreen
	 * @return
	 */
	public static FolienSettings load(int numberOfScreen) {
		return load(Utils.getFileName(numberOfScreen));
	}

	/**
	 * loads the given filename as FolienSettings. The file ending will be attached automatically if not given. The path
	 * will be used from Utils.getDir().<br>
	 * If given one fails, loadDefault is called and returned instead.
	 * 
	 * @param name
	 * @return the loaded FolienSettings.
	 */
	public static FolienSettings load(String name) {
		FolienSettings toReturn = null;
		if (name != null && !"".equals(name)) {
			if (!name.endsWith(Utils.FILE_END)) {
				name = name + Utils.FILE_END;
			}

			try { // try given one
				toReturn = loadFromFile(name);
			} catch (Exception e) {// try "standard" file
				System.err.println("Error when loading from file " + name + ": " + e.getMessage());
				toReturn = loadDefault();
			}
		}
		return toReturn;
	}

	private static FolienSettings loadFromFile(String nameWithOutDir) throws IOException {
		String json = Utils.getFileAsString(Utils.PROFILE_DIR + nameWithOutDir);
		FolienSettings toReturn = converter.fromJson(json);
		toReturn.deleteOldPics();
		return toReturn;
	}

	public void printPics() {
		System.out.printf("[");
		for (Iterator<FilePath> iterator = pics.iterator(); iterator.hasNext();) {
			FilePath myFile = iterator.next();
			System.out.printf("[");
			System.out.printf(myFile.path + " --> " + myFile.label);
			System.out.printf("]" + (iterator.hasNext() ? ", " : ""));
		}
		System.out.println("]");
	}

	private void deleteOldPics() {
		for (Iterator<FilePath> iterator = pics.iterator(); iterator.hasNext();) {
			FilePath myFile = iterator.next();
			if (myFile.path != null && !Utils.fileExists(myFile.path)) {
				System.out.println("Bild " + myFile.label + " gelöscht.");
				iterator.remove();
			}
		}
	}

	/**
	 * Sets selectedHour and selectedMin based on the income.<br>
	 * Intepretes 1 as 5 Minutes
	 * 
	 * @param fiveMinutes
	 */
	public void setMinutes(int fiveMinutes) {
		selectedHour = (int) Math.floor(fiveMinutes / 12);
		selectedMin = (fiveMinutes - (selectedHour * 12)) * 5;
	}

	/**
	 * Returns selectedHour and selectedMin as 5 minutes.<br>
	 * 00:01 will return 5
	 * 
	 * @param fiveMinutes
	 * @return
	 */
	public int getMinutes() {
		return selectedHour * 12 + selectedMin / 5;
	}

	/**
	 * Returns all files that could be load through load(String).<br>
	 * Returns only filenames without extension.
	 * 
	 * @return
	 */
	public static List<String> getLoadAbles() {
		return Utils.getFilesOfDir(Utils.PROFILE_DIR, Utils.FILE_END, true);
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public boolean isCountDownstatus() {
		return countDownstatus;
	}

	public void setCountDownstatus(boolean countDownstatus) {
		this.countDownstatus = countDownstatus;
	}

	public boolean isTimeTriggerStatus() {
		return timeTriggerStatus;
	}

	public void setTimeTriggerStatus(boolean timeTriggerStatus) {
		this.timeTriggerStatus = timeTriggerStatus;
	}

	public Position getPicPosition() {
		return picPosition;
	}

	public void setPicPosition(Position picPosition) {
		this.picPosition = picPosition;
	}

	public Fonts getFont() {
		return font;
	}

	public void setFont(Fonts font) {
		this.font = font;
	}

	public Color getFontColor() {
		return fontColor;
	}

	public void setFontColor(Color fontColor) {
		this.fontColor = fontColor;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public List<FilePath> getPics() {
		return pics;
	}

	public void setPics(List<FilePath> pics) {
		this.pics = pics;
	}

	public int getPicDuration() {
		return picDuration;
	}

	public void setPicDuration(int picDuration) {
		this.picDuration = picDuration;
	}

	public int getSelectedHour() {
		return selectedHour;
	}

	public void setSelectedHour(int selectedHour) {
		this.selectedHour = selectedHour;
	}

	public int getSelectedMin() {
		return selectedMin;
	}

	public void setSelectedMin(int selectedMin) {
		this.selectedMin = selectedMin;
	}

	public int getTimeTrigger() {
		return timeTrigger;
	}

	public void setTimeTrigger(int timeTrigger) {
		this.timeTrigger = timeTrigger;
	}

	public int getPicSizePercent() {
		return picSizePercent;
	}

	public void setPicSizePercent(int picSizePercent) {
		this.picSizePercent = picSizePercent;
	}

	public boolean isFontOutline() {
		return fontOutline;
	}

	public void setFontOutline(boolean fontOutline) {
		this.fontOutline = fontOutline;
	}

	public boolean addPic(List<FilePath> myFile) {
		if (myFile != null && !myFile.isEmpty()) {
			if (getPicsSize() + myFile.size() <= Utils.MAX_Bilder) {
				return pics.addAll(myFile);
			} else if (getPicsSize() < Utils.MAX_Bilder) {
				for (Iterator<FilePath> iterator = myFile.iterator(); iterator.hasNext()
						&& getPicsSize() < Utils.MAX_Bilder;) {
					FilePath filePath = iterator.next();
					if (!filePath.equals(FilePath.NO_FILE)) {
						pics.add(filePath);
					}
				}
				return true;
			}
		}
		return false;
	}

	public int getPicsSize() {
		return pics.size();
	}

	public FilePath removePic(int selected) {
		if (getPicsSize() > selected && selected >= 0)
			return pics.remove(selected);
		return null;
	}

	public void addTimeTrigger(int i) {
		timeTrigger += i;
	}

	/**
	 * Increases the percent size by one.
	 * 
	 * @return false, if already maximum value
	 */
	public boolean sizePercentUp() {
		if (getPicSizePercent() < MAX_PIC_SIZE_PERCENT) {
			picSizePercent++;
			return true;
		}
		return false;
	}

	/**
	 * Decreases the percent size by one.
	 * 
	 * @return false, if already minimum value
	 */
	public boolean sizePercentDown() {
		if (getPicSizePercent() > MIN_PIC_SIZE_PERCENT) {
			picSizePercent--;
			return true;
		}
		return false;
	}

	public void fontSizeUp() {
		fontSize++;
	}

	/**
	 * Decreases the font size by one.
	 * 
	 * @return false, if already minimum value
	 */
	public boolean fontSizeDown() {
		if (getFontSize() > MIN_FONT_SIZE) {
			fontSize--;
			return true;
		}
		return false;
	}

	public void picDurationUp() {
		picDuration++;
	}

	/**
	 * Decreases the pic duration by one.
	 * 
	 * @return false, if already minimum value
	 */
	public boolean picDurationDown() {
		if (getPicDuration() > MIN_PIC_DURATION) {
			picDuration--;
			return true;
		}
		return false;
	}
}

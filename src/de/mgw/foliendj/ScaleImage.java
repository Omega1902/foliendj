/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
//import java.awt.Color.*; // zum Hintergrundfarbe ändern
import java.io.File;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * 
 * @author N R
 * @version 1.0
 */
//import java.util.*;
import javax.imageio.ImageIO;

public class ScaleImage {
	private Image[] img;
	private Dimension[] dim;
	private final int size;

	public ScaleImage(List<FilePath> pics, Dimension screenDim, int picSizePercent) {
		size = pics.size();
		img = new Image[size];
		dim = new Dimension[size];
		int counter = 0;
		for (Iterator<FilePath> iterator = pics.iterator(); iterator.hasNext();) {
			FilePath filePath = iterator.next();
			img[counter] = resizeImage(filePath.path, screenDim, picSizePercent, counter);
			counter++;
		}
	}

	private Image resizeImage(String picAdress, Dimension screendim, int picSizePercent, int position) {
		Image temp = Toolkit.getDefaultToolkit().createImage(picAdress);
		try {
			BufferedImage bild = ImageIO.read(new File(picAdress));
			int width = bild.getWidth();
			int height = bild.getHeight();
			double picRatio = 1.0 * width / height;
			double screenRatio = screendim.getWidth() / (screendim.getHeight() * picSizePercent / 100);
			int screenWidth = (int) screendim.getWidth();
			int screenHeight = (int) screendim.getHeight() * picSizePercent / 100;

			System.out.println("Bild in Prozent: " + picSizePercent + "% Screen-Weite: " + screenWidth
					+ " Screen-Höhe: " + screenHeight + " Bildweite: " + width + " Bildhöhe: " + height);
			if (picRatio == screenRatio) {
				temp = temp.getScaledInstance(screenWidth, screenHeight, Image.SCALE_AREA_AVERAGING);
				dim[position] = new Dimension(screenWidth, screenHeight);
				System.out.println('1');
			} else if (picRatio > screenRatio) {
				int t = (int) (screenWidth / picRatio);
				temp = temp.getScaledInstance(screenWidth, t, Image.SCALE_AREA_AVERAGING);
				dim[position] = new Dimension(screenWidth, t);
				System.out.println('2' + " Weite: " + screenWidth + " Höhe: " + t);
			} else if (picRatio < screenRatio) {
				int t = (int) (screenHeight * picRatio);
				temp = temp.getScaledInstance(t, screenHeight, Image.SCALE_AREA_AVERAGING);
				dim[position] = new Dimension(t, screenHeight);
				System.out.println('3' + " Weite: " + t + " Höhe: " + screenHeight);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
		/*
		 * //Alte Version: Image temp = Toolkit.getDefaultToolkit().createImage(picAdress); temp =
		 * temp.getScaledInstance((int)screendim.getWidth() ,(int)screendim.getHeight() ,Image.SCALE_SMOOTH); return
		 * temp;
		 */
	}

	/**
	 * Returns the requested image
	 * 
	 * @param numberInArray
	 * @return
	 */
	public Image getImage(int numberInArray) {
		if (numberInArray < img.length)
			return img[numberInArray];
		else
			return img[0];
	}

	/**
	 * Returns the dimension of the requested image
	 * 
	 * @param numberInArray
	 * @return
	 */
	public Dimension getDim(int numberInArray) {
		if (numberInArray < dim.length)
			return dim[numberInArray];
		else {
			System.out.println("Fehler bim ermitteln der Bildgröße. Falscher Index");
			return dim[0];
		}
	}

	/**
	 * Returns the size of the array
	 * 
	 * @return
	 */
	public int getSize() {
		return size;
	}
}

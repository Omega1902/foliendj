/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Utils {
	public final static int MAX_Bilder = 10;
	public final static boolean DEBUG = false;
	public final static Charset CHARSET = StandardCharsets.UTF_8;
	public final static String PROGRAM_NAME = "FolienDJ";
	public final static String PROGRAM_NAME_LOWER_CASE = PROGRAM_NAME.toLowerCase();

	public static final String PROFILE_DIR;
	public static final String DATA_DIR;
	public static final String FILE_END = ".json";
	public static final String SYSTEM_FILE_NAME = "reload";
	public static final String FALLBACK_FILE_NAME = "standard";
	public static final String SUB_DIR_SAVE = "profile";
	public static final String PROGRAM_VERSION = (Utils.class.getPackage().getImplementationVersion() != null
			? Utils.class.getPackage().getImplementationVersion() : "NONE");// Only works for jar files,
	static {
		String userHome = System.getProperty("user.home");
		if (System.getProperty("os.name").startsWith("Windows")) {
			// includes: Windows 2000, Windows 95, Windows 98, Windows NT, Windows Vista, Windows XP
			DATA_DIR = userHome + File.separator + "AppData" + File.separator + "Roaming" + File.separator
					+ PROGRAM_NAME_LOWER_CASE + File.separator;
		} else {
			// everything else
			DATA_DIR = userHome + File.separator + "." + PROGRAM_NAME_LOWER_CASE + File.separator;
		}

		PROFILE_DIR = DATA_DIR + SUB_DIR_SAVE + File.separator;
	}

	public static String getFileName(int i) {
		return SYSTEM_FILE_NAME + i + FILE_END;
	}

	public static String getFallbackName() {
		return FALLBACK_FILE_NAME + FILE_END;
	}

	/**
	 * Creates dir if not exists. <br>
	 * Returns <code>false</code> if already exists.
	 * 
	 * @param dirName
	 * @return true, if created
	 * @throws Error if dir cannot be created
	 */
	public static boolean createDirIfNotExists(String dir) {
		File theDir = new File(dir);
		if (!Utils.dirExists(dir)) {
			try {
				theDir.mkdir();
				System.out.println("creating directory: " + theDir.getName());
				return true;
			} catch (SecurityException se) {
				throw new Error("Cannot create save dir", se);
			}
		}
		return false;
	}

	/**
	 * Returns true if the file exists and is a file. <br>
	 * <code>null</code> will return false
	 * 
	 * @param dirName
	 * @return
	 */
	public static boolean fileExists(String dirName) {
		if (dirName == null)
			return false;
		File f = new File(dirName);
		return f.exists() && !f.isDirectory();
	}

	/**
	 * Returns true if the directory exists and is a directory. <br>
	 * <code>null</code> will return false
	 * 
	 * @param dirName
	 * @return
	 */
	public static boolean dirExists(String dirName) {
		if (dirName == null)
			return false;
		return dirExists(new File(dirName));
	}

	/**
	 * Returns true if the directory exists and is a directory. <br>
	 * <code>null</code> will return false
	 * 
	 * @param dirName
	 * @return
	 */
	public static boolean dirExists(File dir) {
		if (dir == null)
			return false;
		return dir.exists() && dir.isDirectory();
	}

	/**
	 * Returns the given file as String
	 * 
	 * @param nameWithDir
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String getFileAsString(String nameWithDir)
			throws UnsupportedEncodingException, FileNotFoundException, IOException {
		File fileDir = new File(nameWithDir);

		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), CHARSET));

		String str;
		StringBuffer json = new StringBuffer();

		while ((str = in.readLine()) != null) {
			json.append(str);
		}

		in.close();
		return json.toString();
	}

	/**
	 * Prints the given file
	 * 
	 * @param nameWithDir
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String getRessourceAsString(String nameWithDir) throws IOException {
		InputStream stream = Utils.class.getClassLoader().getResourceAsStream(nameWithDir);
		BufferedReader in = new BufferedReader(new InputStreamReader(stream, CHARSET));
		
		StringBuilder result = new StringBuilder();
		String str;
		while ((str = in.readLine()) != null) {
			result.append(str + "\n");
		}

		in.close();
		return result.toString();
	}

	/**
	 * Returns files (not directories) of given path.<br>
	 * Internally uses <code>getFilesOfDir(String, String, boolean)</code>
	 * 
	 * @param pathDir
	 * @param endsWith
	 * @param withoutFileEndsWith
	 * @return
	 */
	public static List<String> getFilesOfDir(String pathDir) {
		return getFilesOfDir(pathDir, "", false);
	}

	/**
	 * Returns files (not directories) of given path with given ending. If <code>withoutFileEndsWith</code> is
	 * <code>true</code> this ending will be removed on each file.<br>
	 * Returns <code>null</code> if pathDir is <code>null</code>, does not exist or is not a directory.
	 * 
	 * @param pathDir
	 * @param endsWith
	 * @param withoutFileEndsWith
	 * @return
	 */
	public static List<String> getFilesOfDir(String pathDir, String endsWith, boolean withoutFileEndsWith) {
		List<String> files = null;
		if (pathDir != null) {
			File dir = new File(pathDir);
			if (dirExists(dir)) {
				files = new ArrayList<String>(dir.listFiles().length);
				for (final File fileEntry : dir.listFiles()) {
					if (!fileEntry.isDirectory() && fileEntry.getName().endsWith(endsWith)) {
						if (withoutFileEndsWith) {
							files.add(fileEntry.getName().substring(0,
									fileEntry.getName().length() - Utils.FILE_END.length()));
						} else {
							files.add(fileEntry.getName());
						}
					}
				}
			}
		}
		return files;
	}

	/**
	 * Returns all Files as string of the given directory of the ressources.<br>
	 * Given String should not be lead or followed by a slash!<br>
	 * eg:
	 * <ul>
	 * <li>foo/bar --> OK</li>
	 * <li>/foo/bar --> NOK</li>
	 * <li>foo/bar/ --> NOK</li>
	 * <li>/foo/bar/ --> NOK</li>
	 * </ul>
	 * 
	 * @param path
	 * @return
	 */
	public static List<String> getFilesOfDirRessource(final String path) {
		final File jarFile = new File(Utils.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		List<String> toReturn = null;

		if (jarFile.isFile()) { // Run with JAR file
			JarFile jar = null;
			try {
				jar = new JarFile(jarFile);
				final Enumeration<JarEntry> entries = jar.entries(); // gives ALL entries in jar
				toReturn = new ArrayList<String>();
				final String pathWithSlash = path + "/";
				while (entries.hasMoreElements()) {
					final String name = entries.nextElement().getName();
					if (name.startsWith(pathWithSlash) && !name.equals(pathWithSlash)) { // filter according to the path
						toReturn.add(name.substring(pathWithSlash.length()));
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					jar.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else { // Run with IDE
			final URL url = Utils.class.getResource("/" + path);
			if (url != null) {
				try {
					final File apps = new File(url.toURI());
					toReturn = new ArrayList<String>(apps.listFiles().length);
					for (File app : apps.listFiles()) {
						toReturn.add(app.getName());
					}
				} catch (URISyntaxException ex) {
					// never happens
				}
			}
		}
		return toReturn;
	}

	public static void printDebug(String string) {
		if (Utils.DEBUG) {
			System.out.println("DEBUG INFO " + string);
		}
	}
}

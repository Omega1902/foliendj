/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * 
 * 
 * @author N R
 * @version 1.0
 */
import javax.swing.JPanel;

public class Countdown extends JPanel {
	/** generated */
	private static final long serialVersionUID = -2824982790568186197L;
	private Dimension dim;
	private final Font font;
	private int outlineThickness = 2;
	private boolean timeTriggerSelected;
	private int timeTriggerTime;
	private long sek;
	private int schriftgroesse;
	private Color schriftfarbe = Color.GRAY;
	private Position wunschPosition;
	private int textWidth;
	private int x = 100;
	private int oldX = -10000;
	private int y = 100;
	private int oldY = -10000;
	private String anzeigeString = " ";
	private boolean fontOutline;

	public Countdown(long sekunden, Dimension dim, FolienSettings folienSettings) {
		this.dim = dim;
		this.schriftgroesse = folienSettings.getFontSize();
		this.wunschPosition = folienSettings.getPosition();
		this.fontOutline = folienSettings.isFontOutline();
		this.timeTriggerSelected = folienSettings.isTimeTriggerStatus();
		this.timeTriggerTime = folienSettings.getTimeTrigger();
		this.sek = sekunden;
		font = new Font(folienSettings.getFont().label, Font.PLAIN, schriftgroesse);
		textWidth = schriftgroesse * 2;
		Utils.printDebug("TextWidth init: " + textWidth);
		// t.start();
	}

	public void zeichne() {
		if (sek > 0)
			sek--;

		anzeigeString = countdown(sek);

		repaint();
	}

	private void positionTransform() {
		if (wunschPosition == null) {
			y = -100;
			x = -100;
		} else {
			Utils.printDebug(dim.toString());
			int xIst = (int) dim.getWidth();
			int yIst = (int) dim.getHeight();
			int abstandRand = xIst / 100 * 3;

			switch (wunschPosition) {
			case OBEN_LINKS:
				y = getTop(abstandRand, yIst);
				x = getLeft(abstandRand, xIst);
				break;
			case OBEN_ZENTRIERT:
				y = getTop(abstandRand, yIst);
				x = getCenterH(abstandRand, xIst);
				break;
			case OBEN_RECHTS:
				y = getTop(abstandRand, yIst);
				x = getRight(abstandRand, xIst);
				break;
			case MITTE_LINKS:
				y = getCenterV(abstandRand, yIst);
				x = getLeft(abstandRand, xIst);
				break;
			case MITTE_ZENTRIERT:
				y = getCenterV(abstandRand, yIst);
				x = getCenterH(abstandRand, xIst);
				break;
			case MITTE_RECHTS:
				y = getCenterV(abstandRand, yIst);
				x = getRight(abstandRand, xIst);
				break;
			case UNTEN_LINKS:
				y = getBottom(abstandRand, yIst);
				x = getLeft(abstandRand, xIst);
				break;
			case UNTEN_ZENTRIERT:
				y = getBottom(abstandRand, yIst);
				x = getCenterH(abstandRand, xIst);
				break;
			case UNTEN_RECHTS:
				y = getBottom(abstandRand, yIst);
				x = getRight(abstandRand, xIst);
				break;
			default:
				x = 100;
				y = 100;
				break;
			}
			Utils.printDebug("Countdown x = " + x + " und y = " + y);
			Utils.printDebug("Countdown Schriftgröße = " + schriftgroesse + " und AbstandRand = " + abstandRand);
		}
	}

	private String countdown(long sek_zaehler) {
		int h = (int) Math.floor(sek_zaehler / 3600);
		sek_zaehler -= 3600 * h;
		int min = (int) Math.floor((sek_zaehler) / 60);
		sek_zaehler -= 60 * min;
		int sec = (int) sek_zaehler;
		StringBuilder output = new StringBuilder();
		if (h > 0) {
			output.append(h + ":");
		}
		if (min > 0 || h > 0) {
			if (min <= 9 && h > 0) {
				output.append("0");
			}
			output.append(min + ":");
		}
		if (sec < 10) {
			output.append("0");
		}
		output.append(sec);

		return output.toString();
	}

	public void setSchriftfarbe(Color neueFarbe) {
		schriftfarbe = neueFarbe;
	}

	private int getTop(int abstandRand, int yIst) {
		return schriftgroesse + abstandRand;
	}

	private int getCenterV(int abstandRand, int yIst) {
		return (int) (0.5 * (double) yIst);
	}

	private int getBottom(int abstandRand, int yIst) {
		return yIst - abstandRand;
	}

	private int getLeft(int abstandRand, int xIst) {
		return abstandRand;
	}

	private int getCenterH(int abstandRand, int xIst) {
		return (int) (((double) xIst - (double) textWidth) / 2);
	}

	private int getRight(int abstandRand, int xIst) {
		return xIst - textWidth - abstandRand;
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setFont(font);
		textWidth = g2.getFontMetrics().stringWidth(anzeigeString);
		Utils.printDebug("TextWidth: " + textWidth);
		
		positionTransform();
		//tweack a bit here horizontal
		if(x - oldX < textWidth/32*3){
			x = oldX;
			y = oldY;
		}else{
			oldX = x;
			oldY = y;
		}
		

		if (timeTriggerSelected == false || (timeTriggerSelected == true && timeTriggerTime >= sek)) {
			if (Utils.DEBUG) {
				g2.setColor(Color.RED);
				g2.drawOval(x, y, 4, 4);
			}
			if (fontOutline) {
				g2.setColor(Color.BLACK);
				g2.drawString(anzeigeString, x + outlineThickness/2, y + outlineThickness/2);
				g2.drawString(anzeigeString, x + outlineThickness/2, y - outlineThickness/2);
				g2.drawString(anzeigeString, x - outlineThickness/2, y + outlineThickness/2);
				g2.drawString(anzeigeString, x - outlineThickness/2, y - outlineThickness/2);
				g2.drawString(anzeigeString, x + outlineThickness, y);
				g2.drawString(anzeigeString, x, y - outlineThickness);
				g2.drawString(anzeigeString, x - outlineThickness, y);
				g2.drawString(anzeigeString, x, y + outlineThickness);
			}
			g2.setColor(schriftfarbe);
			g2.drawString(anzeigeString, x, y);
		}
	}
}

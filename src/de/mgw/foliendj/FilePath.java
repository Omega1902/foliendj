/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

import java.io.File;

public class FilePath {
	public final String path;
	public final String label;
	public final static FilePath NO_FILE = new FilePath(null, null);
	
	public FilePath(String path, String label){
		this.path = path;
		this.label = label;
	}
	
	public FilePath(File file) {
		this(file.getPath(),file.getName());
	}

	public boolean equals(Object o){
		if(o == null || !(o instanceof FilePath))
			return false;
		FilePath file = (FilePath) o;
		return equalsNullSave(file.label, label) && equalsNullSave(file.path, path);
	}
	
	public int hashCode(){
		int temp = 5;
		temp = temp * (label == null ? 1 : label.hashCode()) + 7;
		temp = temp * (path == null ? 1 : path.hashCode()) + 31;
		return temp;
	}
	
	/**
	 * Returns equals if the strings are equals. Both <code>null</code> result in <code>true</code>, just one of them <code>null</code> in <code>false</code>. 
	 * @param one First String
	 * @param two Second String
	 * @return <code>true</code>, if both Strings are <code>null</code> or equal
	 */
	private boolean equalsNullSave(String one, String two){
		if(one == two)
			return true;
		if(one == null)
			return false;
		return one.equals(two);
		
	}
	
	public String toString(){
		return label == null ? " " : label;
	}
}

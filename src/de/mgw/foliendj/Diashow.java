/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

/**
 * 
 * 
 * @author N R 
 * @version 1.0
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;

public class Diashow extends JFrame {
	/** generated */
	private static final long serialVersionUID = -7511323996393758083L;
	private static Dimension defaultDim = Toolkit.getDefaultToolkit().getScreenSize();
	private Dimension dim = defaultDim;

	private PicturePanel picPanel = null;
	private Countdown countdownPanel = null;

	private Timer t = new Timer(1000, new TimerListener());
	private int picDuration;
	private int zaehler = 0;

	private JButton cancel = new JButton(" ");

	public Diashow(GraphicsDevice device, FolienSettings folienSettings) {
		super(device.getDefaultConfiguration());
		// System.out.println("Bildschrim-Nr.: " + bildschirm);
		setDim(device);
		System.out.println(dim.toString());
		if (folienSettings.isCountDownstatus()) {
			long timeTo = calcTime(folienSettings.getSelectedHour(), folienSettings.getSelectedMin());
			if (timeTo > 0) {
				countdownPanel = new Countdown(timeTo, dim, folienSettings);
			} else {
				throw new RuntimeException("Fehler bei der Zeiteingabe!");
			}
		} else {
			countdownPanel = null;

		}
		this.picDuration = folienSettings.getPicDuration();
		picPanel = new PicturePanel(dim, folienSettings);
		t.start();
		initialisierung();
	}

	private void initialisierung() {

		// * nötige JPanel hinzufügen
		// *///////////////////////////////////////////////////////////////////

		if (picPanel != null)
			this.add(picPanel);

		if (countdownPanel != null) {
			this.setGlassPane(countdownPanel);
			countdownPanel.setOpaque(false); // Wegen JGlassPane muss Aufruf hier erfolgen
			countdownPanel.setVisible(true); // Wegen JGlassPane muss Aufruf hier erfolgen
		}

		// * Cancel-Button-Setting
		// *//////////////////////////////////////////////////////////////////////
		cancel.addActionListener(new CancelListener());
		cancel.setOpaque(false);
		cancel.setContentAreaFilled(false);
		cancel.setBorderPainted(false);
		add(cancel);

		// * JFrame - Einstellungen
		// *//////////////////////////////////////////////////////////////////////
		setBackground(Color.BLACK);
		setUndecorated(true);
		// setLocation(0,0);
		setExtendedState(JFrame.MAXIMIZED_BOTH);

		setSize(dim);
		toFront();
		setVisible(true);
	}

	public void close() {
		setVisible(false);
		dispose();
	}

	private void setDim(GraphicsDevice bildschirm) {
		new JFrame(bildschirm.getDefaultConfiguration());
		int width = bildschirm.getDisplayMode().getWidth();
		int height = bildschirm.getDisplayMode().getHeight();
		// System.out.println("width = " + width + " height = "+ height);
		dim = new Dimension(width, height);
	}

	/**
	 * Returns how many seconds left to the given time
	 * 
	 * @return
	 */
	private long calcTime(int selectedHour, int selectedMin) {
		int[] time = getDT();
		long sekunden = 60 - time[2];
		long stunden = (selectedHour - time[0]) * 3600;
		long minuten = (selectedMin - 1 - time[1]) * 60;

		if (timeCheck(selectedHour, selectedMin)) {
			sekunden = sekunden + minuten + stunden;
		} else {
			sekunden = -1;
		}
		return sekunden;
	}

	/**
	 * @returns int array with current hour, minute and second
	 */
	public static int[] getDT() {
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		// System.out.println(""+hour24+" : "+min+" : "+ sec+"");
		int[] a = { hour, min, sec };
		return a;
	}

	/**
	 *
	 * Checks if we already passed the given time
	 * 
	 * @param stunde
	 * @param minute
	 * @return false, if that time already passed today
	 */
	public static boolean timeCheck(int stunde, int minute) {
		int[] aktZeit = getDT();
		return ((aktZeit[0] == stunde && minute > aktZeit[1]) || aktZeit[0] < stunde);
	}

	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (picPanel != null && zaehler % picDuration == 0)
				picPanel.zeichne();

			if (countdownPanel != null)
				countdownPanel.zeichne();

			zaehler++;
		}
	}

	private class CancelListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			close();
		}
	}
}

/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

public enum Fonts {
	Agency_FB("Agency FB"), OCR_A_Extended("OCR A Extended"), Poor_Richard("Poor Richard"), Stencil("Stencil"), Vrinda("Vrinda");
	public final String label;
	
	private Fonts(String label) {
		this.label = label;
	}
	
	public static Fonts getFont(String fontLabel){
		for (Fonts pos : Fonts.values()) {
			if(pos.label.equals(fontLabel)){
				return pos;
			}
		}
		throw new RuntimeException("No Font found!");
	}
	
	public static String[] getFontLabels(){
		String[] result = new String[Fonts.values().length];
		for(int i = 0; i < Fonts.values().length; i++)
			result[i] = (Fonts.values())[i].label;
		return result;
	}
	
	public String toString(){
		return label;
	}
}

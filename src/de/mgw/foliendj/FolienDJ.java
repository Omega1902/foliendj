/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

import java.util.List;

import de.mgw.foliendj.gui.Gui;
import de.mgw.foliendj.gui.License;

public class FolienDJ {
	private static final String ownLicense = "LICENSE_FolienDJ";
	private static final String licenseDir = "licenses";
	private static final String licenseBeginning = "LICENSE_";

	public static void main(String[] args) {
		if (args.length > 0) {
			if ("license".equalsIgnoreCase(args[0]) || "licenses".equalsIgnoreCase(args[0])) {
				System.out.println(getLicenses());
			} else if ("version".equalsIgnoreCase(args[0])) {
				System.out.println("Program version: " + Utils.PROGRAM_VERSION);
			} else {
				System.out.println("Unknown argument: \"" + args[0] + "\"");
				System.out.println("FolienDJ will exit now.");
			}
		} else {
			// check folders
			if (!Utils.dirExists(Utils.DATA_DIR)) {
				System.out.println("license necessary");
				License myLicense = new License(getLicenses());
				while (myLicense.isDisplayable()) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (myLicense.isAccepted()){
					Utils.createDirIfNotExists(Utils.DATA_DIR);
					Utils.createDirIfNotExists(Utils.PROFILE_DIR);
					new Gui();
				}
			} else {
				new Gui();
			}
		}
	}

	private static String getLicense(String license) {
		StringBuilder result = new StringBuilder();
		try {
			result.append("\n");
			Utils.printDebug("license path:" + licenseDir + "/" + license);
			result.append(Utils.getRessourceAsString(licenseDir + "/" + license));
			result.append("\n");
		} catch (Exception e) {
			result.append("Seems like the license of " + license.substring(licenseBeginning.length())
					+ " is currently not available.\n");
			if (ownLicense.equals(license))
				result.append("This is licensed under the LGPL v3.");
			e.printStackTrace();
		}
		return result.toString();
	}

	private static String getLicenses() {
		StringBuilder result = new StringBuilder();
		List<String> licensesFiles = Utils.getFilesOfDirRessource(licenseDir);
		if (licensesFiles != null && !licensesFiles.isEmpty()) {
			result.append("Licenses:\n");
			result.append(getLicense(ownLicense) + "\n");
			result.append("Following open source libraries are included:\n");
			for (String string : licensesFiles) {
				if (!ownLicense.equals(string)) {
					result.append(getLicense(string) + "\n");
				}
			}
			result.append("End of licenses.\n");
		} else {
			result.append("Seems like the licenses are currently not available.\n");
		}
		return result.toString();
	}

}

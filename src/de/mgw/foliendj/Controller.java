/**
 * FolienDJ - This program can display pictures on any connected screen, like on a beamer before a show. Countdown timer included
 * Copyright © 2017 Nils Ruppel and Micha Kessler (mckessler@gmx.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.mgw.foliendj;

import java.awt.Color;
import java.util.List;
import java.util.Vector;

public class Controller {
	private Diashow[] show;
	private Screendetection screen;
	private FolienSettings[] settings;
	private String infoMeldung = "";
	public static final String ALLES_OK = "Zur Zeit alles OK!";
	private int selectedScreen;

	public Controller() {
		setInfo(ALLES_OK);
		screen = new Screendetection();
		show = new Diashow[getNumberOfScreens()];
		settings = new FolienSettings[getNumberOfScreens()];
		for (int i = 0; i < getNumberOfScreens(); i++) {
			settings[i] = FolienSettings.load(i);
			settings[i].printPics();
		}
		selectedScreen = 0;
	}

	public void start() {
		if (!isShowing()) {
			// System.out.println("Bildschrim-Nr.: " + bildschirm);
			try {
				show[selectedScreen] = new Diashow(screen.getScreen(selectedScreen), settings[selectedScreen]);
				setInfo(ALLES_OK);
			} catch (RuntimeException re) {
				re.printStackTrace();
				setError(re.getMessage());
			}
		} else {
			setError("Bitte erst die alte Präsenation schließen!");
		}
	}

	public void stop() {
		if (isShowing()) {
			show[selectedScreen].close();
			// show = null;
			setInfo(ALLES_OK);
		} else {
			setError("Fehler beim schließen der Präsentation - Keine Präsentation offen!");
		}
	}

	public int detectScreen() {
		for (int i = 0; i < getNumberOfScreens(); i++) {
			if (isShowing(i))
				show[i].close();
		}
		screen.screenTest();
		show = new Diashow[getNumberOfScreens()];
		if (selectedScreen >= getNumberOfScreens()) {
			setScreen(getNumberOfScreens() - 1);
		}
		if (settings.length > getNumberOfScreens()) {
			FolienSettings[] newSettings = new FolienSettings[getNumberOfScreens()];
			System.arraycopy(settings, 0, newSettings, 0, newSettings.length);
			settings = newSettings;
		} else if (settings.length < getNumberOfScreens()) {
			FolienSettings[] newSettings = new FolienSettings[getNumberOfScreens()];
			System.arraycopy(settings, 0, newSettings, 0, settings.length);
			for (int i = settings.length; i < newSettings.length; i++) {
				newSettings[i] = new FolienSettings();
			}
			settings = newSettings;
		}
		return getNumberOfScreens();
	}

	public int getNumberOfScreens() {
		return screen.getNumberOfScreens();
	}

	private void setInfo(String msg) {
		infoMeldung = "Info: " + msg;
	}

	private void setError(String msg) {
		infoMeldung = "Fehler: " + msg;
	}

	public String getInfo() {
		return infoMeldung;
	}

	public int getFontSize() {
		return settings[selectedScreen].getFontSize();
	}

	public int getPicDuration() {
		return settings[selectedScreen].getPicDuration();
	}

	public int getTimeTrigger() {
		return settings[selectedScreen].getTimeTrigger();
	}

	public Color setFontColor(Color colorChoose) {
		settings[selectedScreen].setFontColor(colorChoose);
		return getFontColor();
	}

	public Color getFontColor() {
		return settings[selectedScreen].getFontColor();
	}

	public void setCountDown(boolean selected) {
		settings[selectedScreen].setCountDownstatus(selected);
	}

	public boolean getCountDown() {
		return settings[selectedScreen].isCountDownstatus();
	}

	public void setTimeTriggerStatus(boolean selected) {
		settings[selectedScreen].setTimeTriggerStatus(selected);
	}

	public boolean getTimeTriggerStatus() {
		return settings[selectedScreen].isTimeTriggerStatus();
	}

	public void setPosition(Position position) {
		settings[selectedScreen].setPosition(position);
	}

	public Position getPosition() {
		return settings[selectedScreen].getPosition();
	}

	public void setFont(Fonts font) {
		settings[selectedScreen].setFont(font);
	}

	public Fonts getFont() {
		return settings[selectedScreen].getFont();
	}

	public Vector<FilePath> picSelect(List<FilePath> myFile) {
		if (myFile != null && !myFile.isEmpty()) {
			if (!settings[selectedScreen].addPic(myFile)) {
				setError("Maximale Anzahl an Bildern (" + Utils.MAX_Bilder + ") erreicht!!!");
			}
		}
		return getPics();
	}

	public Vector<FilePath> picDisselect(int selected) {
		settings[selectedScreen].removePic(selected);
		return getPics();
	}

	public Vector<FilePath> getPics() {
		return new Vector<FilePath>(settings[selectedScreen].getPics());
	}

	public int timeTriggerUp() {
		settings[selectedScreen].addTimeTrigger(60);
		setInfo(ALLES_OK);
		return settings[selectedScreen].getTimeTrigger();
	}

	public int timeTriggerDown() {
		if (settings[selectedScreen].getTimeTrigger() > 60) {
			settings[selectedScreen].addTimeTrigger(-60);
			setInfo(ALLES_OK);
		} else {
			setInfo("Verzögerungszeit kann nicht kleiner als eine Minute gewählt werden!");
		}
		return settings[selectedScreen].getTimeTrigger();
	}

	public int picSizeUp() {
		if (settings[selectedScreen].sizePercentUp()) {
			setInfo(ALLES_OK);
		} else {
			setInfo("Bildgröße kann nicht größer als \"" + FolienSettings.MAX_PIC_SIZE_PERCENT + "\" gewählt werden!");
		}
		return getPicSizePercent();
	}

	public int picSizeDown() {
		if (settings[selectedScreen].sizePercentDown()) {
			setInfo(ALLES_OK);
		} else {
			setInfo("Bilgröße kann nicht kleiner als \"" + FolienSettings.MIN_PIC_SIZE_PERCENT + "\" gewählt werden!");
		}
		return getPicSizePercent();
	}

	public int getPicSizePercent() {
		return settings[selectedScreen].getPicSizePercent();
	}

	public int fontSizeUp() {
		settings[selectedScreen].fontSizeUp();
		setInfo(ALLES_OK);
		return settings[selectedScreen].getFontSize();
	}

	public int fontSizeDown() {
		if (settings[selectedScreen].fontSizeDown()) {
			setInfo(ALLES_OK);
		} else {
			setInfo("Schriftgröße kann nicht kleiner als \"" + FolienSettings.MIN_FONT_SIZE + "\" gewählt werden!");
		}
		return settings[selectedScreen].getFontSize();
	}

	public int picDurationUp() {
		settings[selectedScreen].picDurationUp();
		setInfo(ALLES_OK);
		return settings[selectedScreen].getPicDuration();
	}

	public int picDurationDown() {
		if (settings[selectedScreen].picDurationDown()) {
			setInfo(ALLES_OK);
		} else {
			setInfo("Bildanzeigedauer kann nicht kleiner als \"" + FolienSettings.MIN_PIC_DURATION
					+ "\" Sekunde gewählt werden!");
		}
		return settings[selectedScreen].getPicDuration();
	}

	public void setScreen(int selectedIndex) {
		if (selectedIndex < 0 || selectedIndex >= getNumberOfScreens()) {
			setError("Der Bildschirm " + selectedIndex + " ist nicht verfügbar!");
		} else {
			selectedScreen = selectedIndex;
			setInfo("Falls gewünschter Bildschirm nicht gelistet prüfen ob dieser angeschlossen ist und evtl. \"scan\" betätigen!");
		}
	}

	public boolean isShowing() {
		return isShowing(selectedScreen);
	}

	public boolean isShowing(int screen) {
		return show[screen] != null && show[screen].isVisible();
	}

	public int getSelectedHour() {
		return settings[selectedScreen].getSelectedHour();
	}

	public void setSelectedHour(int hour) {
		settings[selectedScreen].setSelectedHour(hour);
	}

	public int getSelectedMin() {
		return settings[selectedScreen].getSelectedMin();
	}

	public void setSelectedMin(int min) {
		settings[selectedScreen].setSelectedMin(min);
	}

	public void saveAs(String name) {
		if (name == null || name.length() == 0) {
			setError("Speichern fehlgeschlagen. Keinen Namen eingegeben?");
		} else if (name.toLowerCase().startsWith(Utils.SYSTEM_FILE_NAME)) {
			setError("Speichern fehlgeschlagen. Der Name darf nicht mit " + Utils.SYSTEM_FILE_NAME + " anfangen.");
		} else {
			FolienSettings.saveTo(settings[selectedScreen], name);
		}
	}

	public void saveAll() {
		FolienSettings.saveAll(settings);
	}

	public List<String> getLoadAbles() {
		return FolienSettings.getLoadAbles();
	}

	/**
	 * Loads the given profile
	 * 
	 * @param name
	 */
	public void loadFromFile(String name) {
		if (name == null || name.length() == 0) {
			setError("Laden fehlgeschlagen. Keinen Namen ausgewählt?");
		} else {
			settings[selectedScreen] = FolienSettings.load(name);
		}
	}

	public void printPicArray() {
		settings[selectedScreen].printPics();
	}

	public void setMinutes(int fiveMinutes) {
		settings[selectedScreen].setMinutes(fiveMinutes);
	}

	public int getMinutes() {
		return settings[selectedScreen].getMinutes();
	}

	/**
	 * resets the current <code>FolienSettings</code> to default.
	 */
	public void reset() {
		settings[selectedScreen] = FolienSettings.loadDefault();
	}

	public Position getPicPosition() {
		return settings[selectedScreen].getPicPosition();
	}

	public void setPicPosition(Position selectedItem) {
		settings[selectedScreen].setPicPosition(selectedItem);
	}

	public boolean getFontOutline() {
		return settings[selectedScreen].isFontOutline();
	}

	public void setFontOutline(boolean fontOutline) {
		settings[selectedScreen].setFontOutline(fontOutline);
	}

	/**
	 * Returns how many pictures are currently added to pics
	 * 
	 * @return
	 */
	public int getPicsSize() {
		return settings[selectedScreen].getPicsSize();
	}
}
